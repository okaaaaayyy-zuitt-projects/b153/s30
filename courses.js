// fetch("http://localhost:4000/courses")
// .then(res => res.json())
// .then(data => {
// 	// console.log(data[0].name)

// 	// ACTIVITY
// 	// Display a single course name in the header of the HTML via our live data (not hardcoded)

// 	let header = document.querySelector("header")

// 	header.innerHTML = data[0].name

// })

// document refers to the HTML file connected to our JS file. One of the methods that can be used here is querySelector(), which allows us to "get" a specific HTML element (only one at a time)
// let header = document.querySelector("header")

// reassign the value of the HTML code inside of the header to a new value
// header.innerHTML = data[0].name

// console.dir(header)